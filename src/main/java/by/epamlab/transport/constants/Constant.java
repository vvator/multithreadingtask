package by.epamlab.transport.constants;

import java.util.Date;

public final class Constant {
    public static final String EMPTY_STRING = "";
    public static final String TO_STRING_PREFIX = "";
    public static final String TIME_RUNNIND_PROGRAM = new Date()
            .toString().replaceAll(":", "_");

    public static final int INDEX_FIRST_ELEMENT = 0;
    public static final String EXCEPTION_DELIMITER = " =>\n ";
}
