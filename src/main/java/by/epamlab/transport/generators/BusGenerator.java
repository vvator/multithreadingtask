package by.epamlab.transport.generators;

import static by.epamlab.transport.constants.Constant.TO_STRING_PREFIX;

import java.util.ArrayList;
import java.util.List;

import by.epamlab.transport.controller.Station;
import by.epamlab.transport.threads.Bus;

public final class BusGenerator {
    private static final int DEFAULT_NUMBER = 1;
    private static final int DEFAULT_SPEED = 1;
    private static final int DEFAULT_CAPACITY = 1;
    private List<Station> stations = new ArrayList<>();
    private int capacity;
    private int speed;
    private int number;

    public BusGenerator() {
        super();
        this.capacity = DEFAULT_CAPACITY;
        this.speed = DEFAULT_SPEED;
        this.number = DEFAULT_NUMBER;
    }

    public BusGenerator setCapacity(final int busCapacity) {
        this.capacity = busCapacity;
        return this;
    }

    public BusGenerator setSpeed(final int busSpeed) {
        this.speed = busSpeed;
        return this;
    }

    public BusGenerator setStations(final List<Station> stations) {
        this.stations = stations;
        return this;
    }

    public Bus getNewBus() {
        List<Station> newBusStops = new ArrayList<>();
        for (Station station : stations) {
            newBusStops.add(new Station(station));
        }
        return new Bus(TO_STRING_PREFIX + (number++), capacity, speed, newBusStops);
    }
}
