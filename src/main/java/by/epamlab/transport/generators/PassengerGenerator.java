package by.epamlab.transport.generators;

import static by.epamlab.transport.constants.Constant.TO_STRING_PREFIX;

import by.epamlab.transport.controller.Station;
import by.epamlab.transport.controller.Way;
import by.epamlab.transport.threads.Passenger;
import by.epamlab.transport.utils.AppProperties;

public final class PassengerGenerator {
    private static final int DEFAULT_NUMBER = 1;
    private static int number = DEFAULT_NUMBER;

    PassengerGenerator() {
        super();
    }

    private static Passenger newRandomPassenger() {
        Station startStation = Way.getRandomStation();
        Station targetStation = Way.getRandomStation(startStation);
        return new Passenger(TO_STRING_PREFIX + (number++), startStation, targetStation);
    }

    public static void createAndRunPassengers(AppProperties appProps) {
        for (int i = 0; i < appProps.getPassengers(); i++) {
            PassengerGenerator.newRandomPassenger().start();
        }
    }
}
