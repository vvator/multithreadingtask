package by.epamlab.transport.generators;

import by.epamlab.transport.controller.Station;

public final class StationGenerator {

    StationGenerator() {
        super();
    }

    public static Station newStation(final String name) {
        return new Station(name);
    }
}
