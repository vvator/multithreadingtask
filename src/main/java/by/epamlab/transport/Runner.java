package by.epamlab.transport;

import static by.epamlab.transport.constants.Constant.TIME_RUNNIND_PROGRAM;

import org.apache.log4j.Logger;

import by.epamlab.transport.constants.Mode;
import by.epamlab.transport.controller.Way;
import by.epamlab.transport.generators.BusGenerator;
import by.epamlab.transport.generators.PassengerGenerator;
import by.epamlab.transport.utils.AppProperties;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public final class Runner {
    private static final Logger LOG = Logger.getLogger(Runner.class);
    private static AppProperties appProps = new AppProperties();
    private static Mode mode = Mode.CONSOLE;

    Runner() {
        super();
    }

    public static void main(final String[] args) {
        LOG.info("PROGRAM RUN " + TIME_RUNNIND_PROGRAM);
        try {
            mode = Mode.valueOf(appProps.getMode());
        } catch (IllegalArgumentException ex) {
            LOG.error("Illegal mode param");
            mode = Mode.UNKNOWN;
        }
        LOG.info("USED CONFIG PARAM:" + appProps);
        switch (mode) {
        case CONSOLE:
            LOG.debug("CONSOLE MODE");
            consoleRunner(appProps);
            break;
        case GUI:
            LOG.debug("GUI MODE");
            guiMode(appProps);
            break;
        default:
            LOG.error("UNKNOWN PROGRAM MODE");
            break;
        }
    }

    private static void consoleRunner(AppProperties appProps) {
        Way.createWay(appProps.getStations());

        PassengerGenerator.createAndRunPassengers(appProps);

        BusGenerator busGenerator = new BusGenerator();
        busGenerator.setStations(Way.getStations())
                .setCapacity(appProps.getBusCapacity())
                .setSpeed(appProps.getSpeed());
        for (int j = 0; j < appProps.getBuses(); j++) {
            busGenerator.getNewBus().start();
            synchronized (Thread.currentThread()) {
                try {
                    Thread.currentThread().wait(appProps.getInterval());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static void guiMode(AppProperties appProps) {
        throw new NotImplementedException();
    }

}
