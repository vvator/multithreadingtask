package by.epamlab.transport.controller;

import static by.epamlab.transport.constants.Constant.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import by.epamlab.transport.generators.StationGenerator;

public final class Way {
    private static List<Station> way = new ArrayList<>();
    private static Random random = new Random();

    public static void createWay(final int count) {
        for (int i = 0; i < count; i++) {
            way.add(StationGenerator.newStation(TO_STRING_PREFIX + (i + 1)));
        }
    }

    public static Station getGlobalStation(final Station station) {
        return way.get(way.indexOf(station));
    }

    public static Station getRandomStation() {
        return way.get(random.nextInt(way.size()));
    }

    public static Station getRandomStation(final Station station) {
        Station endStation;
        while (true) {
            endStation = getRandomStation();
            if (!endStation.equals(station)) {
                break;
            }
        }
        return endStation;
    }

    public static List<Station> getStations() {
        return way;
    }

    public static boolean hasPassengers() {
        boolean hasPassengers = false;

        Iterator<Station> stationIterator = way.iterator();
        while (stationIterator.hasNext() && !hasPassengers) {
            hasPassengers = stationIterator.next().hasPassengers();
        }
        return hasPassengers;
    }

    public static void setWay(final List<Station> stations) {
        Way.way = stations;
    }

    public static List<Station> getWay() {
        return way;

    }
}
