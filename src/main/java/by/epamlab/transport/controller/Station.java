package by.epamlab.transport.controller;

import static by.epamlab.transport.constants.Constant.TO_STRING_PREFIX;

import java.util.List;

import org.apache.log4j.Logger;

import by.epamlab.transport.synch.SynchObj;
import by.epamlab.transport.threads.Passenger;
import by.epamlab.transport.utils.ConfigLogger;;

public class Station {
    private static final String LOG_PREFIX_NAME = "Station_";
    private static final String LOG_FOLDER = "STATIONS";
    private Logger log;
    private SynchObj synchObj;

    private final String name;
    private boolean isBusOnStation = false;

    private volatile int passengersCount;
    private List<Station> buses;

    public Station(final int name) {
        this(TO_STRING_PREFIX + name);
    }

    public Station(final Station copy) {
        this(copy.name);
    }

    public Station(final String name) {
        this.name = name;
        log = new ConfigLogger().configLog(log, LOG_PREFIX_NAME, name, LOG_FOLDER);
        log.info(LOG_PREFIX_NAME + name + " is creates");
    }

    public boolean enterBus(final Passenger passenger) {
        if (!synchObj.hasFree()) {
            runBus();
            return false;
        }

        Station linkedStation = buses.get(buses.indexOf(passenger.getTargetStation()));
        synchObj.plusCurrentCount();
        passenger.setLinkedStation(linkedStation);
        passengersCount--;
        if (passengersCount <= 0) {
            runBus();
        }
        return true;
    }

    public String getName() {
        return name;
    }

    public Logger getLog() {
        return log;
    }

    public int getPassengersCount() {
        return passengersCount;
    }

    public boolean hasPassengers() {
        return passengersCount > 0;
    }

    public boolean isBusOnStation() {
        return isBusOnStation;
    }

    public synchronized void minusPassengersCount() {
        passengersCount--;
    }

    public boolean parkingBus(final List<Station> busList, final SynchObj synchObj) {
        if (synchObj.hasFree() && passengersCount > 0) {
            this.buses = busList;
            this.synchObj = synchObj;
            isBusOnStation = true;
            return true;
        }
        return false;
    }

    public synchronized void plusPassengersCount() {
        passengersCount++;
    }

    public void runBus() {

        synchObj = null;
        buses = null;
        isBusOnStation = false;
        synchronized (this) {
            this.notifyAll();
        }

    }

    public void setLog(final Logger log) {
        this.log = log;
    }

    @Override
    public String toString() {
        return "Station[" + name + ']';
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Station other = (Station) obj;
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        return true;
    }

}
