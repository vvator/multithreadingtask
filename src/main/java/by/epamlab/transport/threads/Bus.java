package by.epamlab.transport.threads;

import static by.epamlab.transport.constants.Constant.*;

import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

import by.epamlab.transport.controller.Station;
import by.epamlab.transport.controller.Way;
import by.epamlab.transport.synch.SynchObj;
import by.epamlab.transport.utils.ConfigLogger;

public class Bus extends Thread {
    private static final String LOG_PREFIX_NAME = "Bus_";
    private static final String LOG_FOLDER = "BUSSES";
    private final List<Station> stations;

    private Logger log;
    private boolean isWorking = false;
    private Station currentStation;
    private final int maxCapacity;
    private int capacity;
    private int speed;

    public void setSpeed(final int speed) {
        this.speed = speed;
    }

    private SynchObj synchObj;

    public Bus(final String name, final int capacity, final int speed, final List<Station> stations) {
        setName(name);
        log = new ConfigLogger().configLog(log, LOG_PREFIX_NAME, name, LOG_FOLDER);
        log.info(LOG_PREFIX_NAME + getName() + " is created");
        this.maxCapacity = capacity;
        this.speed = speed;
        this.stations = stations;
        this.capacity = 0;
        this.currentStation = this.stations.get(INDEX_FIRST_ELEMENT);
        this.synchObj = new SynchObj(maxCapacity);
        this.synchObj.setLogBUS(log);

    }

    @Override
    public void run() {
        Station globalCurrentStation;
        while (!isWorking) {
            synchronized (this) {
                try {
                    wait(10000 / speed);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            globalCurrentStation = Way.getGlobalStation(currentStation);
            synchronized (globalCurrentStation) {
                while (globalCurrentStation.isBusOnStation()) {
                    try {
                        globalCurrentStation.wait();
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
                log.info(this + " on " + currentStation);
                globalCurrentStation.notifyAll();
                synchronized (currentStation) {
                    currentStation.notifyAll();
                    int countWaitPassenger = currentStation.getPassengersCount();
                    for (int i = 0; i < countWaitPassenger; i++) {
                        synchObj.minusCurrentCount();
                        currentStation.minusPassengersCount();
                    }
                }
                if (globalCurrentStation.parkingBus(stations, synchObj)) {
                    try {
                        globalCurrentStation.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            capacity = maxCapacity - synchObj.getMaxCount();
            int newBusIndex = stations.indexOf(currentStation) + 1;
            if (newBusIndex >= stations.size()) {
                if (Way.hasPassengers() || capacity > 0) {
                    Collections.reverse(stations);
                } else {
                    isWorking = true;
                }
            } else {
                currentStation = stations.get(newBusIndex);
            }
        }
        log.info(this + " work finall");

    }

    public int getSpeed() {
        return speed;
    }

    @Override
    public String toString() {
        return "Bus[" + getName() + ']';
    }

}
