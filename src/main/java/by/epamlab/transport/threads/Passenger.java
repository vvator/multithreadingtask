package by.epamlab.transport.threads;

import org.apache.log4j.Logger;

import by.epamlab.transport.controller.Station;
import by.epamlab.transport.utils.ConfigLogger;

public class Passenger extends Thread {
    private static final String LOG_PREFIX_NAME = "Passenger_";
    private static final String LOG_FOLDER = "PASSENGERS";
    private final Station startStation;
    private final Station targetStation;
    private Logger log;
    private Station linkedStation;
    private boolean sleep = true;
    private boolean traveling = true;

    public Passenger(final String name, final Station startStation, final Station targetStation) {
        setName(name);
        this.startStation = startStation;
        this.targetStation = targetStation;
        log = new ConfigLogger().configLog(log, LOG_PREFIX_NAME, name, LOG_FOLDER);
        log.info(getName() + " is created");
    }

    public void endWay() {
        traveling = false;
        log.info(this + " end");
    }

    public Logger getLog() {
        return log;
    }

    public Station getTargetStation() {
        return targetStation;
    }

    @Override
    public void run() {
        startStation.plusPassengersCount();
        while (sleep) {
            synchronized (startStation) {
                while (!startStation.isBusOnStation()) {
                    try {
                        log.info("wait bus on station");
                        startStation.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if (startStation.enterBus(this)) {
                    log.info("Enter the bus");
                    startWay();
                }

            }
        }
        while (traveling) {

            synchronized (linkedStation) {

                try {
                    linkedStation.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            traveling = false;
            endWay();
        }
    }

    public void setLinkedStation(final Station linkedStation) {
        this.linkedStation = linkedStation;
    }

    public void setLog(Logger log) {
        this.log = log;
    }

    public void startWay() {
        linkedStation.plusPassengersCount();
        sleep = false;
        traveling = true;
        log.info("Passenger[" + getName() + "] : " + startStation + " - " + targetStation);
    }

    @Override
    public String toString() {
        return "Passenger[" + getName() + "]";
    }
}
