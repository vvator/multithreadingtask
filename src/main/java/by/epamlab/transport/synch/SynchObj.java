package by.epamlab.transport.synch;

import org.apache.log4j.Logger;

public class SynchObj {
    private Logger logBUS;
    private Logger logPassenger;

    private int currentCount;
    private int maxCount;

    public boolean hasFree() {
        int free = (maxCount - currentCount);
        if (free > 0) {
            return true;
        } else {
            logBUS.info("In the bus not is free places");
            return false;
        }
    }

    public SynchObj(final int max) {
        this.maxCount = max;
    }

    public void plusCurrentCount() {
        logBUS.info("in");
        currentCount++;
    }

    public void minusCurrentCount() {
        logBUS.info("out");
        currentCount--;
    }

    public int getCurrentCount() {
        return currentCount;
    }

    public int getMaxCount() {
        return maxCount - currentCount;
    }

    public Logger getLogBUS() {
        return logBUS;
    }

    public void setLogBUS(Logger logBUS) {
        this.logBUS = logBUS;
    }

    public Logger getLogPassenger() {
        return logPassenger;
    }

    public void setLogPassenger(Logger logPassenger) {
        this.logPassenger = logPassenger;
    }

}
