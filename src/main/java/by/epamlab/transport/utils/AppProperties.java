package by.epamlab.transport.utils;

import java.util.ResourceBundle;

public final class AppProperties {

    private static final String FILE_CONFIG_PROPERTYES = "config";
    private static final String PROPERTI_INTERVAL = "interval";
    private static final String PROPERTI_PASSENGERS = "passengers";
    private static final String PROPERTI_BUSCAPACITY = "buscapacity";
    private static final String PROPERTI_BUSES = "buses";
    private static final String PROPERTI_STATIONS = "stations";
    private static final String PROPERTI_SPEED = "speed";
    private static final String PROPERTI_MODE = "mode";

    private int interval;
    private int passengers;
    private int busCapacity;
    private int buses;
    private int stations;
    private int speed;
    private String mode;

    public AppProperties() {
        super();
        ResourceBundle rb = ResourceBundle.getBundle(FILE_CONFIG_PROPERTYES);
        interval = Integer.parseInt(rb.getString(PROPERTI_INTERVAL));
        passengers = Integer.parseInt(rb.getString(PROPERTI_PASSENGERS));
        busCapacity = Integer.parseInt(rb.getString(PROPERTI_BUSCAPACITY));
        buses = Integer.parseInt(rb.getString(PROPERTI_BUSES));
        stations = Integer.parseInt(rb.getString(PROPERTI_STATIONS));
        speed = Integer.parseInt(rb.getString(PROPERTI_SPEED));
        mode = rb.getString(PROPERTI_MODE).toUpperCase();

    }

    private AppProperties(final int interval, final int passengers, final int buscapacity, final int buses, final int stations, final int speed, final String mode) {
        super();
        this.interval = interval;
        this.passengers = passengers;
        this.busCapacity = buscapacity;
        this.buses = buses;
        this.stations = stations;
        this.speed = speed;
        this.mode = mode;
    }

    public class AppPropertiesBuilder {

        public AppPropertiesBuilder() {
            super();
        }

        public AppProperties build() {
            return new AppProperties();
        }

        public AppPropertiesBuilder setInterval(final int intervalParam) {
            interval = intervalParam;
            return this;
        }

        public AppPropertiesBuilder setPassengers(final int passengersParam) {
            passengers = passengersParam;
            return this;
        }

        public AppPropertiesBuilder setBusCapacity(final int buscapacityParam) {
            busCapacity = buscapacityParam;
            return this;
        }

        public AppPropertiesBuilder setBuses(final int busesParam) {
            buses = busesParam;
            return this;
        }

        public AppPropertiesBuilder setStations(final int stationsParam) {
            stations = stationsParam;
            return this;
        }

        public AppPropertiesBuilder setSpeed(final int speedParam) {
            speed = speedParam;
            return this;
        }

        public AppPropertiesBuilder setMode(final String modeParam) {
            mode = modeParam;
            return this;
        }
    }

    public int getInterval() {
        return interval;
    }

    public int getPassengers() {
        return passengers;
    }

    public int getBusCapacity() {
        return busCapacity;
    }

    public int getBuses() {
        return buses;
    }

    public int getStations() {
        return stations;
    }

    public int getSpeed() {
        return speed;
    }

    public String getMode() {
        return mode;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("\n[\n");
        builder.append(" interval=");
        builder.append(interval);
        builder.append(";\n passengers=");
        builder.append(passengers);
        builder.append(";\n buscapacity=");
        builder.append(busCapacity);
        builder.append(";\n buses=");
        builder.append(buses);
        builder.append(";\n stations=");
        builder.append(stations);
        builder.append(";\n speed=");
        builder.append(speed);
        builder.append(";\n mode=");
        builder.append(mode);
        builder.append(";\n]");
        return builder.toString();
    }

}
