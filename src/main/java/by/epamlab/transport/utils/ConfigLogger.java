package by.epamlab.transport.utils;

import static by.epamlab.transport.constants.Constant.EXCEPTION_DELIMITER;
import static by.epamlab.transport.constants.Constant.TIME_RUNNIND_PROGRAM;

import java.io.IOException;

import org.apache.log4j.EnhancedPatternLayout;
import org.apache.log4j.Layout;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.RollingFileAppender;

public class ConfigLogger {


    private static final String LOG_ROOT_FOLDER = "logs";
    private static final String LOG_EXTENSION = ".log";
    private static final String PATH_DELIMITER = "/";

    private static final String LOG_FORMAT_MESSAGE = "%d | %-5p |%-20.20c{1}: %m%n";

    public Logger configLog(Logger log, final String prefixName, final String nameObject, final String folderLog) {
        String loggerName = prefixName + nameObject;
        try {
            log = Logger.getLogger(loggerName);
            Layout l = new EnhancedPatternLayout(LOG_FORMAT_MESSAGE);
            log.addAppender(new RollingFileAppender(l, LOG_ROOT_FOLDER + PATH_DELIMITER + TIME_RUNNIND_PROGRAM + PATH_DELIMITER + folderLog + PATH_DELIMITER + loggerName + LOG_EXTENSION));
            log.setLevel(Level.INFO);
        } catch (IOException ex) {
            log.error("PARAMS LOGGER IS NOT SET FOR: " + nameObject + EXCEPTION_DELIMITER + ex);
        }
        return log;
    }

}
